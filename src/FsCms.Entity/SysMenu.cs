﻿using FsCms.Entity.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace FsCms.Entity
{
    [Serializable]
    public class SysMenu : BaseEntity
    {
        /// <summary>
        /// 菜单名称
        /// </summary>
        [Required]
        [StringLength(25)]
        [Display(Name = "菜单名称")]
        public string MenuName { get; set; }

        /// <summary>
        /// 父菜单ID
        /// </summary>
        [StringLength(36)]
        [Display(Name = "父菜单ID")]
        public long? ParentID { get; set; }

        /// <summary>
        /// 菜单描述
        /// </summary>
        [StringLength(250)]
        [Display(Name = "菜单描述")]
        public string Description { get; set; }

        /// <summary>
        /// 菜单地址
        /// </summary>
        [StringLength(50)]
        [RegularExpression(@"^\/[A-Za-z]+\/[A-Za-z]+\/[A-Za-z]+$",
 ErrorMessage = "请输入正确的地址格式格式\n示例：/Sys/Dictionary/Index")]
        [Display(Name = "菜单地址")]
        public string MenuUrl { get; set; }

        /// <summary>
        /// 菜单图标地址
        /// </summary>
        [StringLength(50)]
        [Display(Name = "菜单图标地址")]
        public string IconUrl { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Display(Name = "排序")]
        public int? Sort { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        [StringLength(50)]
        [Display(Name = "操作人")]
        public string UpdateBy { get; set; }

        /// <summary>
        /// 操作时间
        /// </summary>
        [Display(Name = "操作时间")]
        public DateTime? UpdateDt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<SysRole> SysRoles { get; set; }
    }

    /// <summary>
    /// 类型树形结构
    /// </summary>
    public class SysMenuTreeNode : TreeNode
    {
        public SysMenuTreeNode()
        {

        }


        //构造函数自动转换层级
        public SysMenuTreeNode(List<SysMenu> list, SysMenu t)
        {
            this.id = t.Id.ToString();
            this.name = t.MenuName;
            this.pid = t.ParentID.ToString();
            this.desc = t.Description;
            this.createdt = t.CreateDt;
            this.children = (from p in list
                             where p.ParentID == t.Id
                             select new SysMenuTreeNode(list, p) { }).ToList();
        }

        /// <summary>
        /// 子集节点
        /// </summary>
        public List<SysMenuTreeNode> children { get; set; }

        /// <summary>
        /// 标签
        /// </summary>
        public string desc { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? createdt { get; set; }

    }
}


