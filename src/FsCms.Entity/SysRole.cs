﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FsCms.Entity
{
    public class SysRole : BaseEntity
    {
        [Required]
        [StringLength(50)]
        [Display(Name = "角色编码")]
        public string RoleCode { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "角色名称")]
        public string RoleName { get; set; }

        [StringLength(500)]
        [Display(Name = "描述")]
        public string Description { get; set; }

        [StringLength(50)]
        [Display(Name = "修改人")]
        public string UpdateBy { get; set; }

        [Display(Name = "修改时间")]
        public DateTime? UpdateDt { get; set; }
        /// <summary>
        /// 所属商家
        /// </summary>
        [Display(Name = "商家编号")]
        public long? GarageID { get; set; }
        /// <summary>
        /// 商家名称
        /// </summary>
        [Display(Name = "商家名称")]
        public string GarageName { get; set; }

        [Display(Name = "来源类型 =1 商家添加 否则为商家添加")]
        public int OriginType { get; set; }


        public List<SysRole> SysRoles { get; set; }

        public List<SysUser> SysUsers { get; set; }

    }
}


