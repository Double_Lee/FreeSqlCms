﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FsCms.Entity
{
    public class SysUser : BaseEntity
    {
        [Required]
        [StringLength(50)]
        [Display(Name = "用户名")]
        public string UserName { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "密码")]
        public string Password { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "姓名")]
        public string RealName { get; set; }

        [StringLength(200)]
        [Display(Name = "头像")]
        public string Img { get; set; }

        [Display(Name = "用户类别")]
        public int? UserType { get; set; }

        [StringLength(50)]
        [RegularExpression(@"^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$")]
        [Display(Name = "邮箱")]
        public string Email { get; set; }

        [StringLength(15)]
        [Display(Name = "联系电话")]
        public string MobilePhone { get; set; }

        [StringLength(36)]
        [Display(Name = "所属组织")]
        public string OrgID { get; set; }

        [StringLength(50)]
        [Display(Name = "最后访问IP")]
        public string LastViewIP { get; set; }

        [Display(Name = "最后访问时间")]
        public DateTime? LastViewDate { get; set; }

        [StringLength(50)]
        [Display(Name = "修改人")]
        public string UpdateBy { get; set; }

        [Display(Name = "修改时间")]
        public DateTime? UpdateDt { get; set; }

        [Display(Name = "微信OpenId")]
        public string OpenId { get; set; }

        [Display(Name = "备注")]
        public string Remark { get; set; }

        public List<SysRole> SysRoles { get; set; }
    }
}


