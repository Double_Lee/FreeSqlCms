﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FsCms.Entity
{
    public class SysUserRole : BaseEntity
    {
        [Display(Name = "角色ID")]
        public int RoleId { get; set; }

        [Display(Name = "用户ID")]
        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public SysUser User { get; set; }

        [ForeignKey("RoleId")]
        public SysRole Role { get; set; }
    }
}


