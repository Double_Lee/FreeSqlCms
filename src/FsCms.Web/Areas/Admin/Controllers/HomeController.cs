﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace FsCms.Web.Areas.Admin.Controllers
{
    [Microsoft.AspNetCore.Authorization.Authorize(Roles = "admin,system")]
    [Area("Admin")]
    public class HomeController : AdminBaseController1
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
