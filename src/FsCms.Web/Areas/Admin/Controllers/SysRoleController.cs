﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FsCms.Entity;
using FsCms.Service;
using FsCms.Service.DAL;
using FsCms.Entity.Common;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FsCms.Web.Areas.Admin.Controllers
{
    [Microsoft.AspNetCore.Authorization.Authorize(Roles = "admin,system")]
    [Area("Admin")]
    public class SysRoleController : AdminBaseController1
    {
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> List(string searchContent, string seniorQueryJson, int page = 1, int limit = 10, string sidx = "CreateDt", string sord = "desc")
        {
            try
            {
                SysRole query = null;
                if (!string.IsNullOrEmpty(seniorQueryJson))
                {
                    query = Newtonsoft.Json.JsonConvert.DeserializeObject<SysRole>(seniorQueryJson);
                }
                Expression<Func<SysRole, bool>> predicate = ExpressionBuilder.True<SysRole>();
                predicate = predicate.And(b => b.Id > 0);

                if (searchContent != null)
                {
                    predicate = predicate.And(b => b.RoleName.IndexOf(searchContent) != -1);
                }
                PageInfo pageinfo = new PageInfo { };
                (List<SysRole> list, long count) datas = await new SysRoleDAL().Query(predicate, null, pageinfo);

                var lists = datas.list;
                return lists.GetJson<SysRole>(sidx, sord, page, limit, SysTool.GetPropertyNameArray<SysRole>());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ActionResult> CreateModule(string id)
        {
            (List<ArticleType> list, long count) articles = await new ArticleTypeDAL().Query(w => w.Status == 1);
            ViewBag.ArticleTypeList = articles.list.Select(s => new SelectListItem { Text = s.TypeName, Value = s.Id.ToString() }).ToList();
            SysRole model = new SysRole() { };
            return View(model);
        }

        public async Task<ActionResult> UpdateModule(string id)
        {
            (List<ArticleType> list, long count) articles = await new ArticleTypeDAL().Query(w => w.Status == 1);
            ViewBag.ArticleTypeList = articles.list.Select(s => new SelectListItem { Text = s.TypeName, Value = s.Id.ToString() }).ToList();

            SysRole model = new SysRole() { };
            if (!string.IsNullOrEmpty(id) && id != "0")
            {
                int _id = Convert.ToInt32(id);
                model = await new SysRoleDAL().GetByOne(w => w.Id == _id);
            }
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody]SysRole model)
        {
            var resdata = await AutoException.Excute<long>(async (result) =>
               {
                   model.CreateBy = "admin";
                   model.CreateDt = DateTime.Now;
                   result.Data = await new SysRoleDAL().Insert(model);
                   if (result.Data == 0)
                   {
                       throw new Exception("数据新增异常，JSON:" + Newtonsoft.Json.JsonConvert.SerializeObject(model));
                   }
               }, false);
            return Json(resdata);
        }

        [HttpPost]
        public async Task<ActionResult> Update([FromBody]SysRole model)
        {
            var resdata = await AutoException.Excute<SysRole>(async (result) =>
            {
                model.UpdateBy = "admin";
                model.UpdateDt = DateTime.Now;
                var res = await new SysRoleDAL().Update(model);
                result.Data = model;
                if (!res)
                {
                    throw new Exception("数据修改异常，JSON:" + Newtonsoft.Json.JsonConvert.SerializeObject(model));
                }
            }, false);
            return Json(resdata);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(string id)
        {
            var resdata = await AutoException.Excute<long>(async (result) =>
             {
                 string[] idstr = id.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                 foreach (var item in idstr)
                 {
                     var bl = await new SysRoleDAL().Delete(Convert.ToInt32(item));
                     if (!bl) throw new Exception("数据删除异常，ID:" + item);
                 }
             }, true);
            return Json(resdata);
        }

    }
}
